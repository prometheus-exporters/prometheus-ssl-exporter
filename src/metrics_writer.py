from prometheus_client import CollectorRegistry, Gauge
from collections import namedtuple

class MetricsWriter:

  def __init__(self):
    self.registry = CollectorRegistry()
    self.gna = Gauge('ssl_not_after_timestamp', 'Certificate expiration in unix time stlye', registry=self.registry)
    self.gnb = Gauge('ssl_not_before_timestamp', 'Certificate not Before in unix time style', registry=self.registry)
    self.HostInfo = namedtuple(field_names='cert hostname peername', typename='HostInfo')

  def write(self, hostinfo):
    not_before_unix_timestamp = hostinfo.cert.not_valid_before.strftime("%s")
    not_after_unix_timestamp = hostinfo.cert.not_valid_after.strftime("%s")
    self.gna.set(not_after_unix_timestamp)
    self.gnb.set(not_before_unix_timestamp)
