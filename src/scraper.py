from OpenSSL import SSL
from cryptography import x509
from cryptography.x509.oid import NameOID
import socket
import idna
import datetime

class SSLChecker:

  def get_certificate(self, hostname, port):
    hostname_idna = idna.encode(hostname)
    sock = socket.socket()
    sock.settimeout(1.5)
    try:
      sock.connect((hostname, port))
    except socket.timeout:
      raise TimeoutError
    sock.setblocking(1)

    peername = sock.getpeername()
    ctx = SSL.Context(SSL.SSLv23_METHOD) # most compatible
    ctx.verify_mode = SSL.VERIFY_NONE


    sock_ssl = SSL.Connection(ctx, sock)
    sock_ssl.set_connect_state()
    sock_ssl.set_tlsext_host_name(hostname_idna)
    sock_ssl.do_handshake()
    cert = sock_ssl.get_peer_certificate()
    crypto_cert = cert.to_cryptography()
    sock_ssl.close()
    sock.close()
    
    return (crypto_cert, peername, hostname)

    def get_alt_names(cert):
      try:
        ext = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return ext.value.get_values_for_type(x509.DNSName)
      except x509.ExtensionNotFound:
        return None

    def get_common_name(cert):
      try:
        names = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
      except x509.ExtensionNotFound:
        return None

    def get_issuer(cert):
      try:
        names = cert.issuer.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
      except x509.ExtensionNotFound:
        return None
