from wsgiref.simple_server import make_server

class HttpServer:

  def __init__(self, addr, port):
    self.addr = addr
    self.port = port

  def create(self, app):
    self.httpd = make_server(self.addr, self.port, app)

  def run(self):
    self.httpd.serve_forever()

  def single_request(self):
    self.httpd.handle_request()
