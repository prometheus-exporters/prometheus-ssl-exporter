# SSL Exporter

Blackbox exporter monitors **notAfter** and **notBefore** SSL certificates values on
entire SSL certificates chain. This behavior may be useful but in some cases we
would prefer retrieve values on last certificate. This python ssl exporter
handles it.

## Installation

### From repository

```bash
pip install git+https://gitlab.com/myelefant1/prometheus-ssl-exporter
```

You can install from a python virtualenv.

## Running

```bash
# Get all flags
prometheus-ssl-exporter -h

# Specify listen address
prometheus-ssl-exporter --listen-address 127.0.0.1:12345 # default 0.0.0.0:9133
```

## Check results

Visiting http://localhost:9133/probe?target=https://www.example.org will return
metrics concerning last certificate on `www.example.org` on port `443`.

### Metrics

| Metric name              | Information                                    |
|--------------------------|------------------------------------------------|
| ssl_not_after_timestamp  | Certificate expiration date in unix time style |
| ssl_not_before_timestamp | Certificate not Before in unix time style      |

## Prometheus Configuration

You can specify a scheme and/or specify a port number. The port number overrides
the given scheme when two are specified. Default scheme is `https`.

An exception is raised when you specify an unknown scheme (without port number).
The current known schemes are:

| scheme | default port number |
|--------|---------------------|
| https  | 443                 |
| ldaps  | 636                 |

```yaml
scrape_configs:
  - job_name: 'ssl-certificates'
    metrics_path: /probe
    static_configs:
    - targets:
      - www.example.org # Check certificate on 443
      - www.example.org:344 # Check certificate on 344
      - https://www.example.org # Check certificate on 443
      - https://www.example.org:4433 # Check certificate on 4433
      - ldaps://www.example.org # Check certificate on 636
      - what://www.example.org:22222 # Check certificate on 22222
      - what://www.example.org # Error
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 127.0.0.1:9133
```
